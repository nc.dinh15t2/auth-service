const userDao = require('../dao/user_dao')
const {comparePassword, generateToken} = require('../util')


module.exports = {
    async login(body) {
        const user = await userDao.findUserByUsernameOrEmail(body.username, body.email);
        if(!user) throw new BadRequestException('Cannot find user');
        const passwordTrue = await comparePassword(body.password, user.password);
        if(!passwordTrue) throw new BadRequestException('Password wrong')
        const token = generateToken({id: user._id})
        return {
            token: token
        }
    }
}