class BaseException extends Error {
    constructor(msg, status, code = 9999, devMessage = 'No message') {
        super(msg)
        this.status = status
        this.code = code
        this.devMessage = devMessage
    }
}

class BadRequestException extends BaseException {
    constructor(msg='', code, devMessage) {
        super(msg, 400, code, devMessage);
    }
}

class UnauthorizedException extends BaseException {
    constructor(msg='Unauthorized', code, devMessage) {
        super(msg, 401, code, devMessage);
    }
}

class ForbiddenException extends BaseException {
    constructor(msg='Forbidden', code, devMessage) {
        super(msg, 403, code, devMessage);
    }
}

class InternalServerException extends BaseException {
    constructor(msg='Something wrong with server', code, devMessage) {
        super(msg, 500, code, devMessage);
    }
}

module.exports = {
    BaseException,
    UnauthorizedException,
    ForbiddenException,
    BadRequestException,
    InternalServerException
}