const express = require('express');
const { login } = require('../service/auth_service');
const router = express.Router();
const userService = require('../service/auth_service')

router.post('/login', (req, res, next) => {
    login(req.body).then(result => res.json(result)).catch(err => next(err))
})


module.exports = router