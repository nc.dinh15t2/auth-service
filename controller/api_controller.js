const express = require('express');
const router = express.Router();
const authController = require('./auth_controller');
const userController = require('./user_controller')

router.use('/auth', authController)
router.use('/users', userController)

module.exports = router