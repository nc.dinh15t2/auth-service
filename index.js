const express = require('express');
const { exceptionHandler, authenticator } = require('./middleware');
const app = express()
const apiController = require('./controller/api_controller')
const dotenv = require('dotenv')
dotenv.config()

app.get('/test', (req, res) => {
    res.end('Login page');
})

app.use(authenticator)
app.use('/api', apiController)

app.use(exceptionHandler)

const port = process.env.PORT || 8003
const server = app.listen(port, () => {
    console.log('listen', port)
})