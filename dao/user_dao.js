const { mongodb } = require("./base_dao")

module.exports = {
    findUserByUsernameOrEmail(username, email) {
        return new Promise((resolve, reject) => {
            mongodb((err, db) => {
                if(err) return reject(err);
                const userCollection = db.collection('users');
                const query = {
                    $or: [
                        {username: username},
                        {email: email}
                    ]
                }
                userCollection.findOne(query, (err, result) => {
                    if(err) return reject(err);
                    resolve(result);                    
                })
            })
        })
    }
}