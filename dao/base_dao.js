const MongoClient = require('mongodb').MongoClient

module.exports = {
    mongodb(callback) {
        MongoClient.connect(process.env.MONGO_URL, (err, mongo) => {
            if(err) return callback(err);
            const db = mongo.db(process.env.MONGO_DB)
            callback(null, db, () => db.close())
        })
    }
}