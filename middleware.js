const { BaseException } = require("./exceptions")

function exceptionHandler(err, req, res, next) {
    const msg = {
        status: err.status,
        message: err.msg,
    }
    if(process.env.DEV_MODE) {
        msg.code = err.code
        msg.dev_message = err.devMessage
    }
    if (err instanceof BaseException) {
        res.status(err.status)
        res.json(msg)
    } else {
        res.status(500)
        res.json(msg)
    }
}

function authenticator(req, res, next) {
    next()
}

function authenticated(req, res, next) {
    next()
}

module.exports = {
    exceptionHandler,
    authenticated,
    authenticator
}