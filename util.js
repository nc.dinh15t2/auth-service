const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

module.exports = {
    encodePassword(password) {
        return new Promise((resolve, reject) => {
            bcrypt.hash(password, 12, (err, encrypted) => {
                if(err) return reject(err);
                resolve(encrypted);
            })
        })
    },
    comparePassword(password, encrypted) {
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, encrypted, (err, result) => {
                if(err) return reject(err);
                resolve(result);
            })
        })
    },
    generateToken(payload) {
        return jwt.sign(payload, process.env.TOKEN_SALT)
    },
    verifyToken(token) {
        return jwt.verify(token, process.env.TOKEN_SALT)
    }
}